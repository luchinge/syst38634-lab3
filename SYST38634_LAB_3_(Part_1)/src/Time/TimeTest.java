package Time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	
	private String timeToTest;
	
	public TimeTest() {};
	
	/*
	public TimeTest(String dataTime) 
	{
		timeToTest = dataTime;
	}	
	*/
	
	
	
	
	
	@Test //Test One - Success
	public void testGetMilliSeconds() {
		int milli = Time.getMilliSeconds("12:05:05:05");
		System.out.println( milli );
		assertTrue("Milliseconds were NOT calculated properly.", milli == 5);
	}	
	//Test Two - Failure (three digits in the millisecond area).
	@Test (expected = NumberFormatException.class)
	public void testGetMilliSecondsException() {
		int milli = Time.getMilliSeconds("12:05:05:5405");
		System.out.println( milli );
		assertTrue("Milliseconds were NOT calculated properly.", milli == 5);
	}
	//Test Three - BOUNDARY TEST IN <- SUCCEEDS BARELY (Limit is 999).
	@Test
	public void testGetMilliSecondsBoundaryIn() {
		int milli = Time.getMilliSeconds("12:05:05:999");
		System.out.println( milli );
		assertTrue("Milliseconds were NOT calculated properly.", milli == 999);
	}		
	//Test Four - BOUNDARY TEST OUT
	@Test (expected = NumberFormatException.class)
	public void testGetMilliSecondsBoundaryOut() {
		int milli = Time.getMilliSeconds("12:05:05:1000");
		System.out.println( milli );
		assertTrue("Milliseconds were NOT calculated properly.", milli == 0);
	}		
	
	
	
	
	
	
	/*
	
	@Test
	public void testGetTotalSeconds() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetSeconds() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalMinutes() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTotalHours() {
		fail("Not yet implemented");
	}
	*/
}
