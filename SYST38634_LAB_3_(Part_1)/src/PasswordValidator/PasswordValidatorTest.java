/* SYST38634: LAB 3 GIT TEST*/

package PasswordValidator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Time.Time;

public class PasswordValidatorTest {
	public PasswordValidatorTest() {
		
	}
	
	//"CheckPasswordLength()" TESTS ============================================
	@Test //TEST 1 - SUCCESS TEST: 10 characters
	public void testCheckPasswordLength() {
		boolean isValidPassword = PasswordValidator.checkPasswordLength("SuccessPass");
		System.out.println(isValidPassword);
		assertTrue("Test 1A - Password NOT long enough.", isValidPassword == true);
	}
	
	//TEST 2 - FAIL TEST: 4
	@Test (expected = AssertionError.class)
	public void testCheckPasswordLengthException() {
		boolean isValidPassword = PasswordValidator.checkPasswordLength("Fail");
		System.out.println(isValidPassword);
		assertTrue("Test 2A - Password NOT long enough.", isValidPassword == true);
	}	
	
	@Test //TEST 3 - BOUNDARY-IN SUCCESS TEST: 8 (barely passes)
	public void testCheckPasswordLengthBoundaryIn() {
		boolean isValidPassword = PasswordValidator.checkPasswordLength("Success!");
		System.out.println(isValidPassword);
		assertTrue("Test 3A - Password NOT long enough.", isValidPassword == true);
	}		

	//TEST 4 - BOUNDARY-OUT FAIL TEST: 7
	@Test (expected = AssertionError.class)
	public void testCheckPasswordLengthBoundaryOut() {
		boolean isValidPassword = PasswordValidator.checkPasswordLength("Failure");
		System.out.println(isValidPassword);
		assertTrue("Test 4A - Password NOT long enough.", isValidPassword == true);
	}
	
	
	
	//"CheckPasswordNumberOfDigits()" TESTS ====================================	
	@Test //TEST 1 - SUCCESS TEST: 4 digits
	public void testCheckPasswordNumberOfDigits() {
		boolean isValidDigits = PasswordValidator.checkPasswordNumberOfDigits
				("Success1234");
		System.out.println(isValidDigits);
		assertTrue("Test 1B - NOT enough DIGITS in password!", isValidDigits == true);
	}
	
	//TEST 2 - FAIL TEST: 0
	@Test (expected = AssertionError.class)
	public void testCheckPasswordNumberOfDigitsException() {
		boolean isValidDigits = PasswordValidator.checkPasswordNumberOfDigits
				("Fail");
		System.out.println(isValidDigits);
		assertTrue("Test 2B - NOT enough DIGITS in password!", isValidDigits == true);
	}	
	
	@Test //TEST 3 - BOUNDARY-IN SUCCESS TEST: 2 (barely passes)
	public void testCheckPasswordNumberOfDigitsBoundaryIn() {
		boolean isValidDigits = PasswordValidator.checkPasswordNumberOfDigits
				("Success12");
		System.out.println(isValidDigits);
		assertTrue("Test 3B - NOT enough DIGITS in password!", isValidDigits == true);
	}		

	//TEST 4 - BOUNDARY-OUT FAIL TEST: 1
	@Test (expected = AssertionError.class)
	public void testCheckPasswordNumberOfDigitsBoundaryOut() {
		boolean isValidDigits = PasswordValidator.checkPasswordNumberOfDigits
				("Failure1");
		System.out.println(isValidDigits);
		assertTrue("Test 4B - NOT enough DIGITS in password!", isValidDigits == true);
	}	
	
	
	
	
	//==========================================================================
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
}
