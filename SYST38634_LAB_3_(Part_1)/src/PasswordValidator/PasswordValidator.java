/* SYST38634: LAB 3  */

package PasswordValidator;

public class PasswordValidator {
	public static void main(String[] args) {
		
	}
	
	
	public static boolean checkPasswordLength(String password) {
		if(password.length() >= 8) {
			return true;
		}
		else {
			return false;		
		}
	}
	
	
	public static boolean checkPasswordNumberOfDigits(String password) {
		int digitCounter = 0;
		for(int x = 0; x < password.length(); x++) {
			if(Character.isDigit(password.charAt(x))) {
				digitCounter++; //Increment if character is a digit.
			}
		}
		
		if(digitCounter >= 2) {
			return true;			
		}
		else {
			return false;
		} 
	}
	
	//Make test class and test suite.
	//a). checkPasswordLength b). checkPasswordNumberOfDigits
	//Write four test methods for each method -- success, exception, boundaryIn, boundaryOut
	
	
}
